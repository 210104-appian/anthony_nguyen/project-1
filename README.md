# Expense Reimbursement App

## Project Description

This project is an expense reimbursement application using a database to manage authentication of users and the creation and reading of reimbursement requests. Employees will be able to login, and then submit and view their reimbursement requests from the dashboard. If they manage any employees, they can approve or deny their pending requests.

## Technologies Used

* HTML5
* CSS3
* Bootstrap
* Java 15
* Oracle SQL
* AJAX
* JDBC
* Servlets
* Tomcat


## Features

List of features ready and TODOs for future development
* Uses a database hosted on Amazon Web Services to hold data
* Implements a login system only allowing authenticated users
* Allows authenticated employees to make expense reimbursement requests
* Allows authenticated employees to see their pending requests
* Allows authenticated employees to approve or deny requests for employees they manage

## Getting Started
  
First, clone this project using the git command: "git clone https://gitlab.com/210104-appian/training-repos/training-content.git"
Next, in Spring Tool Suite, open the project folder that was cloned to your machine. Create a new server using Tomcat v9.0 Server and add this project. Start the server that was just created and go to localhost:8080 in your browser of choice.


## Usage

Once you are at the login page, you will need a valid login. This application does not have a signup function and requires new users to be added to the system directly. Therefore, a sample user of "gwashington" and a password of "abc" has been created for demonstration purposes. Once you are logged in, you can submit reimbursement requests, view your requests, and manage requests of your managed employees. If you need to logout, there is a logout link near the top to take you back to the login screen.

## Contributors

N/A

## Licenses
This project uses the following license: [MIT License](https://gitlab.com/210104-appian/jahdiel_evans/project-1-ers-app/-/blob/master/LICENSE).