package dev.ant.models;

import java.util.ArrayList;

public class Expense {
	
	private int expenseId;
	private String expenseDescription;
	private String expenseStatus;
	private int employeeId;
	private int managerId;
	private double expenseAmount;
	
	public Expense () {
		super();
	}
	
//	public Expense(expenseStatus status) {
//		super();
//		this.status = status;
//	}
	

	public int expenseId() {
		return expenseId;
	}

	public void setexpenseId(int expenseId) {
		this.expenseId = expenseId;
	}

	public int getemployeeId() {
		return employeeId;
	}

	public void setemployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	public int getmanagerId() {
		return managerId;
	}

	public void setmanagerId(int managerId) {
		this.managerId = managerId;
	}
	
	public String getexpenseStatus() {
		return expenseStatus;
	}
	
	public void setexpenseStatus(String expenseStatus) {
		this.expenseStatus = expenseStatus;
	}

//	public ExpenseStatus getexpenseStatus() {
//		return getexpenseStatus;
//	}

//	public void setexpenseStatus(ExpenseStatus getexpenseStatus) {
//		this.getexpenseStatus = getexpenseStatus;
//	}

	@Override
	public String toString() {
		return "Expense [id= " + expenseId + ", expenseDescription" + expenseDescription + ", expenseStatus=" + expenseStatus + ", employeeId" + employeeId + ", managerId" + managerId + ", expenseAmount" + expenseAmount + "]";
	}

}
