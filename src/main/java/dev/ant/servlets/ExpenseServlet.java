package dev.ant.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import dev.ant.services.ExpenseService;
import dev.ant.services.LoginService;


public class ExpenseServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private ExpenseService expRes = new ExpenseService();
	protected String input1 = "102";
	protected String input2 = "101";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("GET request from Expense Servlet");
		List<String> e = expRes.getEmployeeId(input1);
//		System.out.println(e);
//		String eJson = new Gson().toJson(e);
		ObjectMapper om = new ObjectMapper();
		String eJson = om.writeValueAsString(e);
		System.out.println("Expense Request Information submitted by Employee ID 102");
		System.out.println(eJson);
		System.out.println("");
		List<String> m = expRes.getManagedEmployees(input2);
		String mJson = om.writeValueAsString(m);
		System.out.println("Usernames of employees that made expense requests managed by Employee ID 101");
		System.out.println(mJson);
		
//		String delims = "[ .,?!]+";
//		String [] splitEJson = eJson.split(delims);
//		System.out.println(splitEJson[0]);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// order + order items will be in request body, so we need to obtain the request body from the HttpServletRequest
		System.out.println("POST request from Expense Servlet");
		// use the ObjectMapper to convert string -> java object 
		
		// send java object to service which will pass that object to the DAO and persist to our DB 
		
		// configure HttpServletResponse with appropriate status (201 - created), possibly 400/500 depending on what kind of issue
	}
}
