package dev.ant.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import dev.ant.services.LoginService;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private LoginService loginId = new LoginService();
	protected String input1 = "gwashington";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("GET request from Login Servlet");	
		System.out.println("Retrieving the username of an employee given their ID");
		String userId = loginId.getEmployeeId(input1);
		System.out.println(userId);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// order + order items will be in request body, so we need to obtain the request body from the HttpServletRequest
		System.out.println("POST request from Expense Servlet");
		// use the ObjectMapper to convert string -> java object 
		
		// send java object to service which will pass that object to the DAO and persist to our DB 
		
		// configure HttpServletResponse with appropriate status (201 - created), possibly 400/500 depending on what kind of issue
	}
}
