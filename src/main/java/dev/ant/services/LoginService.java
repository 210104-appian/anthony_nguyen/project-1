package dev.ant.services;

import dev.ant.models.Employee;
import dev.ant.daos.LoginDao;
import dev.ant.daos.LoginDaoImpl;


public class LoginService {
	
	private LoginDao loginDao = new LoginDaoImpl();
	
	public String getEmployeeId(String uName) {
		return loginDao.getEmpId(uName);
	}
}
