package dev.ant.services;

import java.util.List;
import dev.ant.daos.ExpenseDao;
import dev.ant.daos.ExpenseDaoImpl;

public class ExpenseService {
	
	private ExpenseDao expenseDao = new ExpenseDaoImpl();
	
	public List<String> getEmployeeId(String empId) {
		return expenseDao.getExpense(empId);
	}
	public List<String> getManagedEmployees(String empId) {
		return expenseDao.getManagerList(empId);
	}
}