package dev.ant.daos;

import java.util.List;

import dev.ant.models.Employee;

public interface LoginDao {

	public String getEmpId(String uName) ;
	
}