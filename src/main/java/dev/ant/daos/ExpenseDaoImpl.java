package dev.ant.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.ant.util.ConnectionUtil;

public class ExpenseDaoImpl implements ExpenseDao{
	public List<String> getExpense(String empId) {
		String sql = "SELECT * FROM EXPENSE WHERE EMPLOYEE_ID = " + empId;
		List<String> types = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getHardCodedConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				types.add(rs.getString("EXPENSE_ID"));
				types.add(rs.getString("DESCRIPTION"));
				types.add(rs.getString("STATUS"));
				types.add(rs.getString("EMPLOYEE_ID"));
				types.add(rs.getString("MANAGER_ID"));
				types.add(rs.getString("AMOUNT"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return types;
	}
	
	public List<String> getManagerList(String empId) {
		String sql = "SELECT USERNAME FROM EMPLOYEE LEFT JOIN EXPENSE ON EMPLOYEE.EMPLOYEE_ID = EXPENSE.EMPLOYEE_ID WHERE MANAGER_ID =" + empId;
		List<String> types = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getHardCodedConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				types.add(rs.getString("USERNAME"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return types;
	}
}
