package dev.ant.daos;

import java.util.List;

import dev.ant.models.Expense;

public interface ExpenseDao {
	public List<String> getExpense(String empId);
	public List<String> getManagerList(String empId);
}
